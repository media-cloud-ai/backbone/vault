FROM alpine:3.11
ARG TAG

# hadolint ignore=DL3018
RUN mkdir /vault && \
    apk --no-cache add \
    bash \
    ca-certificates \
    wget && \
    wget --quiet --output-document=/tmp/vault.zip \
    "https://releases.hashicorp.com/vault/${TAG}/vault_${TAG}_linux_amd64.zip" && \
    unzip /tmp/vault.zip -d /vault && \
    rm -f /tmp/vault.zip && \
    chmod +x /vault

ENV PATH="PATH=$PATH:$PWD/vault"

EXPOSE 8200

ENTRYPOINT ["vault"]

